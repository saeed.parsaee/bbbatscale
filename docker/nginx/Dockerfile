ARG VERSION=1.19.6
ARG BBBATSCALE_IMAGE

FROM alpine AS build
ARG VERSION

WORKDIR /build

RUN apk add git build-base pcre-dev zlib-dev \
\
    && git clone https://github.com/nginx-shib/nginx-http-shibboleth.git \
    && git clone https://github.com/openresty/headers-more-nginx-module.git \
\
    && wget https://nginx.org/download/nginx-${VERSION}.tar.gz \
    && tar -xzf nginx-${VERSION}.tar.gz \
\
    && cd /build/nginx-${VERSION} \
    && ./configure --with-compat \
         --add-dynamic-module=/build/headers-more-nginx-module \
         --add-dynamic-module=/build/nginx-http-shibboleth \
    && make modules \
    && mkdir -p /build/out/modules \
    && mv objs/ngx_http_headers_more_filter_module.so objs/ngx_http_shibboleth_module.so /build/out/modules/

FROM $BBBATSCALE_IMAGE AS bbbatscale

FROM nginx:${VERSION}-alpine
COPY --from=build /build/out/modules/ /etc/nginx/modules/
COPY --from=build /build/nginx-http-shibboleth/includes/ /etc/nginx/include/
COPY --from=bbbatscale /django-project/staticfiles/ /var/www/bbbatscale/

COPY 10-select-config.sh 30-envsubst-on-include-templates.sh /docker-entrypoint.d/
RUN rm /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh

COPY nginx.conf /etc/nginx/
COPY conf.d/ /etc/nginx/conf.d-available/

COPY include/default_options.nginx /etc/nginx/include/default_options
COPY include/http_options.nginx.template /etc/nginx/templates-include/http_options.template
COPY include/https_options.nginx.template /etc/nginx/templates-include/https_options.template
COPY include/https_redirect.nginx /etc/nginx/include/https_redirect
COPY include/locations_shibboleth.nginx /etc/nginx/include/locations_shibboleth
COPY include/locations_static.nginx /etc/nginx/include/locations_static
COPY include/locations_unprotected.nginx /etc/nginx/include/locations_unprotected
COPY include/map_connection_upgrade.nginx /etc/nginx/include/map_connection_upgrade
COPY include/proxy.nginx.template /etc/nginx/templates-include/proxy.template

RUN rm /etc/nginx/conf.d/* \
    && chmod 0777 /etc/nginx/conf.d /etc/nginx/include \
    && touch /etc/nginx/include/additional_server_config \
    && chmod 0666 /etc/nginx/include/additional_server_config

USER nginx:nginx

ENV NGINX_X_FORWARDED_PROTO=\$scheme

EXPOSE 8080
EXPOSE 8443
