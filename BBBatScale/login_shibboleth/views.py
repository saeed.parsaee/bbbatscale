import re

from django.conf import settings
from django.contrib import auth
from django.contrib.auth import BACKEND_SESSION_KEY
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from login_shibboleth.mapping import SHIBBOLETH_SESSION_ID_KEY

SHIBBOLETH_SP_LOGIN_URL = "/Shibboleth.sso/Login"
SHIBBOLETH_SP_LOGOUT_URL = "/Shibboleth.sso/Logout"


def login_shibboleth(request: HttpRequest):
    if request.user.is_anonymous and SHIBBOLETH_SESSION_ID_KEY in request.META:
        # Invoke logout because the SP/IdP has logged the user in but has not provided enough information.
        return HttpResponseRedirect(SHIBBOLETH_SP_LOGOUT_URL)
    else:
        return HttpResponseRedirect(reverse("home"))


def logout_shibboleth(request: HttpRequest):
    if (
        request.user.is_authenticated
        and request.session[BACKEND_SESSION_KEY] == "login_shibboleth.backends.ShibbolethRemoteUserBackend"
    ):
        auth.logout(request)
        return HttpResponseRedirect(SHIBBOLETH_SP_LOGOUT_URL)
    return HttpResponseRedirect(reverse("home"))


def discovery_service(request: HttpRequest):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse("home"))

    return_address = settings.BASE_URL
    if return_address.endswith("/"):
        return_address = return_address[0:-1]
    return_address = return_address + SHIBBOLETH_SP_LOGIN_URL
    return_address = re.sub(r"[.*+\-?^${}()|[\]\\]", r"\\\g<0>", return_address, 0)
    return_address = "^" + return_address + ".*$"

    return render(request, "login_shibboleth/discovery_service.html", {"return_address": return_address})
