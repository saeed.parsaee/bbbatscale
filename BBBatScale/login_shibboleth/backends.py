from typing import List, Optional

from django.conf import settings
from django.contrib.auth.backends import RemoteUserBackend
from django.contrib.auth.models import AbstractUser, Group
from django.http import HttpRequest
from login_shibboleth.mapping import (
    SHIBBOLETH_BBBATSCALE_GROUPS_KEY,
    SHIBBOLETH_DISPLAY_NAME_KEY,
    SHIBBOLETH_EMAIL_KEY,
    SHIBBOLETH_FIRST_NAME_KEY,
    SHIBBOLETH_LAST_NAME_KEY,
    SHIBBOLETH_SESSION_ID_KEY,
)


def decode_shibboleth_header(header: str) -> str:
    # HTTP headers are historically encoded as ISO-8859-1, but shibboleth encodes them as UTF-8,
    # thus the headers must be re-decoded with UTF-8.
    return header.encode("ISO-8859-1").decode("UTF-8")


class ShibbolethRemoteUserBackend(RemoteUserBackend):
    def authenticate(self, request: HttpRequest, remote_user: str) -> Optional[AbstractUser]:
        if SHIBBOLETH_SESSION_ID_KEY in request.META:
            user = super().authenticate(request, remote_user)
            if user:
                self.update_user(request, user)
            return user
        else:
            return None

    def clean_username(self, username: str) -> str:
        # convert the username to lower, since it is case insensitive
        return decode_shibboleth_header(username).lower()

    @staticmethod
    def update_user(request: HttpRequest, user: AbstractUser) -> None:
        if SHIBBOLETH_FIRST_NAME_KEY in request.META:
            user.first_name = decode_shibboleth_header(request.META[SHIBBOLETH_FIRST_NAME_KEY])

        if SHIBBOLETH_LAST_NAME_KEY in request.META:
            user.last_name = decode_shibboleth_header(request.META[SHIBBOLETH_LAST_NAME_KEY])

        if SHIBBOLETH_DISPLAY_NAME_KEY in request.META:
            user.display_name = decode_shibboleth_header(request.META[SHIBBOLETH_DISPLAY_NAME_KEY])

        if SHIBBOLETH_EMAIL_KEY in request.META:
            user.email = decode_shibboleth_header(request.META[SHIBBOLETH_EMAIL_KEY])

        if SHIBBOLETH_BBBATSCALE_GROUPS_KEY in request.META:
            groups: List[str] = decode_shibboleth_header(request.META[SHIBBOLETH_BBBATSCALE_GROUPS_KEY]).split(";")

            if "admins" in groups:
                user.is_staff = True
            if "moderators" in groups:
                moderators_group = Group.objects.get_or_create(name=settings.MODERATORS_GROUP)[0]
                user.groups.add(moderators_group)
            if "supporters" in groups:
                supporters_group = Group.objects.get_or_create(name=settings.SUPPORTERS_GROUP)[0]
                user.groups.add(supporters_group)

        user.save()
