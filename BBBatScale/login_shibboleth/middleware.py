from django.contrib.auth.middleware import RemoteUserMiddleware
from login_shibboleth.mapping import SHIBBOLETH_USERNAME_KEY


class ShibbolethRemoteUserMiddleware(RemoteUserMiddleware):
    header = SHIBBOLETH_USERNAME_KEY
