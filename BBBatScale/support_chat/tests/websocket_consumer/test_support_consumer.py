import pytest
from channels.db import database_sync_to_async
from django.contrib.auth.models import AnonymousUser, Group, User
from support_chat.models import Supporter
from support_chat.tests.websocket_consumer.conftest import (
    WebsocketCommunicator,
    assert_connection_refuse,
    assert_error,
    assert_illegal_request,
    chat_consumer,
    create_chat,
    create_supporter_user,
    fill_chat,
    support_consumer,
)
from utils.websockets import Error


async def assert_get_chats(support_communicator: WebsocketCommunicator, user: User, moderator: User) -> None:
    await support_communicator.send_json_to({"type": "getChats"})
    assert await support_communicator.receive_nothing(timeout=1)

    async with create_chat(user) as user_chat:
        async with create_chat(moderator) as moderator_chat:
            async with fill_chat(user_chat, user, support_communicator.user, (1, 0)) as user_messages:
                async with fill_chat(
                    moderator_chat, moderator, support_communicator.user, (1, 0)
                ) as moderator_messages:
                    await support_communicator.send_json_to({"type": "getChats"})

                    expectations = {
                        user.username: {
                            "type": "chat",
                            "chatOwner": user.username,
                            "chatOwnerRealName": str(user),
                            "chatOwnerIsModerator": False,
                            "unreadMessages": 1,
                            "isSupportActive": False,
                            "message": user_messages[0].message,
                            "timestamp": user_messages[0].timestamp.isoformat(),
                        },
                        moderator.username: {
                            "type": "chat",
                            "chatOwner": moderator.username,
                            "chatOwnerRealName": str(moderator),
                            "chatOwnerIsModerator": True,
                            "unreadMessages": 1,
                            "isSupportActive": False,
                            "message": moderator_messages[0].message,
                            "timestamp": moderator_messages[0].timestamp.isoformat(),
                        },
                    }

                    while expectations:
                        response = await support_communicator.receive_json_from()
                        assert response == expectations.pop(response["chatOwner"])


@pytest.mark.asyncio
async def test_access_support_without_login() -> None:
    support_communicator_anonymous_user = support_consumer(AnonymousUser())

    await assert_connection_refuse(support_communicator_anonymous_user, Error.LOGIN_REQUIRED)


@pytest.mark.asyncio
async def test_access_support_without_permission(user: User) -> None:
    support_communicator_user = support_consumer(user)

    await assert_connection_refuse(support_communicator_user, Error.SUPPORTER_REQUIRED)


@pytest.mark.asyncio
async def test_access_support_with_login(inactive_supporter_user: User) -> None:
    async with support_consumer(inactive_supporter_user):
        pass


@pytest.mark.asyncio
async def test_illegal_requests_inactive_supporter_user(inactive_supporter_user: User) -> None:
    async with support_consumer(inactive_supporter_user) as support_communicator_inactive_supporter_user:
        await assert_illegal_request(support_communicator_inactive_supporter_user)


@pytest.mark.asyncio
async def test_illegal_requests_supporter(active_supporter_user: User) -> None:
    async with support_consumer(active_supporter_user) as support_communicator_supporter:
        await assert_illegal_request(support_communicator_supporter)


@pytest.mark.asyncio
async def test_support_inactive_get_chats(inactive_supporter_user: User, user: User, moderator: User) -> None:
    async with support_consumer(inactive_supporter_user) as support_communicator_inactive_supporter_user:
        await assert_get_chats(support_communicator_inactive_supporter_user, user, moderator)


@pytest.mark.asyncio
async def test_support_active_get_chats(active_supporter_user: User, user: User, moderator: User) -> None:
    async with support_consumer(active_supporter_user) as support_communicator_supporter:
        await assert_get_chats(support_communicator_supporter, user, moderator)


@pytest.mark.asyncio
async def test_support_inactive_get_status(inactive_supporter_user: User) -> None:
    async with support_consumer(inactive_supporter_user) as support_communicator_inactive_supporter_user:
        await support_communicator_inactive_supporter_user.send_json_to({"type": "getStatus"})
        assert await support_communicator_inactive_supporter_user.receive_json_from() == {
            "type": "status",
            "status": "inactive",
        }


@pytest.mark.asyncio
async def test_support_active_get_status(active_supporter_user: User) -> None:
    async with support_consumer(active_supporter_user) as support_communicator_supporter:
        await support_communicator_supporter.send_json_to({"type": "getStatus"})
        assert await support_communicator_supporter.receive_json_from() == {"type": "status", "status": "active"}


@pytest.mark.asyncio
async def test_set_status(user: User, inactive_supporter_user: User, supporter_group: Group) -> None:
    async def assert_set_status(is_active: bool, support_communicator: WebsocketCommunicator) -> None:
        status = "active" if is_active else "inactive"
        await support_communicator.send_json_to({"type": "setStatus", "status": status})
        assert await support_communicator.receive_json_from() == {"type": "status", "status": status}
        _supporter = await database_sync_to_async(Supporter.get)(support_communicator.user)
        assert _supporter is not None
        assert _supporter.is_active == is_active

        await support_communicator.send_json_to({"type": "setStatus", "status": "I am neither active nor inactive!"})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_active == is_active

        await support_communicator.send_json_to({"type": "setStatus", "status": 42})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_active == is_active

        await support_communicator.send_json_to({"type": "setStatus"})
        await assert_error(support_communicator, Error.MALFORMED_REQUEST)
        await database_sync_to_async(_supporter.refresh_from_db)()
        assert _supporter.is_active == is_active

    async with chat_consumer(user) as chat_communicator_user:
        async with support_consumer(inactive_supporter_user) as support_communicator_inactive_supporter_user:
            await assert_set_status(True, support_communicator_inactive_supporter_user)
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}

            async with create_supporter_user(supporter_group, active=True) as active_supporter_user:
                async with support_consumer(active_supporter_user) as support_communicator_active_supporter_user:
                    await assert_set_status(False, support_communicator_active_supporter_user)
                    await assert_set_status(True, support_communicator_active_supporter_user)

            assert await chat_communicator_user.receive_nothing(timeout=1)

            await assert_set_status(False, support_communicator_inactive_supporter_user)
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

            await assert_set_status(True, support_communicator_inactive_supporter_user)
            assert await chat_communicator_user.receive_json_from() == {"type": "supportOnline"}


@pytest.mark.asyncio
async def test_inactivate_all_supporters(user: User, staff_user: User, supporter_group: Group) -> None:
    async with chat_consumer(user) as chat_communicator_user:
        async with support_consumer(staff_user) as support_communicator_staff_user:
            async with create_supporter_user(supporter_group, active=True) as active_supporter_user:
                async with support_consumer(active_supporter_user) as support_communicator_active_supporter_user:
                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 1

                    await support_communicator_staff_user.send_json_to({"type": "inactivateAllSupporters"})
                    assert await support_communicator_active_supporter_user.receive_json_from() == {
                        "type": "status",
                        "status": "inactive",
                    }
                    assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 0

            async with create_supporter_user(supporter_group, active=True) as active_supporter_user:
                async with support_consumer(active_supporter_user) as support_communicator_active_supporter_user:
                    await support_communicator_staff_user.send_json_to({"type": "setStatus", "status": "active"})
                    assert await support_communicator_staff_user.receive_json_from() == {
                        "type": "status",
                        "status": "active",
                    }

                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 2

                    await support_communicator_staff_user.send_json_to({"type": "inactivateAllSupporters"})
                    assert await support_communicator_staff_user.receive_json_from() == {
                        "type": "status",
                        "status": "inactive",
                    }
                    assert await support_communicator_active_supporter_user.receive_json_from() == {
                        "type": "status",
                        "status": "inactive",
                    }
                    assert await chat_communicator_user.receive_json_from() == {"type": "supportOffline"}

                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 0

            async with create_supporter_user(supporter_group, active=True) as active_supporter_user:
                async with support_consumer(active_supporter_user) as support_communicator_active_supporter_user:
                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 1

                    await support_communicator_active_supporter_user.send_json_to({"type": "inactivateAllSupporters"})
                    await assert_error(support_communicator_active_supporter_user, Error.STAFF_REQUIRED)

                    assert await database_sync_to_async(lambda: Supporter.objects.filter(is_active=True).count())() == 1
