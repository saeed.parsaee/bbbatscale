# encoding: utf-8
import logging

from core.management.commands.collect_server_stats import Command as NewCommand
from django.core.management.base import BaseCommand
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    # Todo remove command for next major release
    help = "DEPRECATED perform health check on all servers"

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.warning(
            "Deprecated command collect_instances_stats was called. This command will be "
            + "removed with next major release. New command is called collect_server_stats."
        )
        NewCommand().handle(*args, **options)
