import logging

from core.forms import GeneralParametersForm
from core.models import GeneralParameter
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def settings_edit(request):
    logger.info(create_view_access_logging_message(request))

    form = GeneralParametersForm(request.POST)
    logger.debug("General Settings form %s", form)

    if request.method == "POST" and form.is_valid():
        logger.debug("request.method == 'POST' and form.is_valid()")
        form.save()

        success_msg = _("Settings successfully saved.")
        messages.success(request, success_msg)
        logger.debug("Success '%s' was set.", success_msg)

        return redirect("settings_edit")
    else:
        logger.debug("NOT request.method == 'POST' and form.is_valid()")
        form = GeneralParametersForm(instance=GeneralParameter.load())
        return render(request, "settings.html", {"form": form})
