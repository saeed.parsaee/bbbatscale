import logging
from urllib.parse import quote

from core.models import GeneralParameter, Meeting
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
def recordings_list(request):
    logger.info(create_view_access_logging_message(request))

    recordings = Meeting.objects.filter(replay_id__isnull=False)
    if not request.user.is_staff:
        recordings = recordings.filter(
            Q(creator=f"{request.user.username}")
            | Q(creator=f"{request.user.last_name}, {request.user.first_name}")
            | Q(creator=f"{request.user.display_name}")
            | Q(creator=f"{request.user.last_name}")
            | Q(creator=f"{request.user.first_name}")
            | Q(creator=f"{request.user.email}")
        )
    return render(request, "recordings_overview.html", {"base_url": settings.BASE_URL, "recordings": recordings})


def recording_redirect(request, replay_id):
    logger.info(create_view_access_logging_message(request, replay_id))

    if request.user.is_authenticated:
        meeting = get_object_or_404(Meeting, replay_id=replay_id)
        gp = GeneralParameter.load()
        return redirect(gp.playback_url + meeting.replay_id)
    else:
        messages.warning(request, _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        return redirect(settings.LOGIN_URL + "?next=" + quote(request.path_info, safe=""))


def recording_download_redirect(request, replay_id):
    if request.user.is_authenticated:
        meeting = get_object_or_404(Meeting, replay_id=replay_id)
        gp = GeneralParameter.load()
        mp4url = gp.download_url.replace("{meetingID}", meeting.replay_id)
        return redirect(mp4url)
    else:
        messages.warning(request, _("The meeting is not allowed for guest entry. Please login to access the meeting."))
        logger.debug("Meeting is not allowed for guest entry. Please login to access the meeting.")

        return redirect(settings.LOGIN_URL + "?next=" + quote(request.path_info, safe=""))
