import logging

from core.filters import ServerFilter
from core.forms import ServerFilterFormHelper, ServerForm
from core.models import Server
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def servers_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = ServerFilter(request.GET, queryset=Server.objects.all().prefetch_related("tenant"))
    f.form.helper = ServerFilterFormHelper
    return render(request, "servers_overview.html", {"filter": f})


@login_required
@staff_member_required
def server_create(request):
    logger.info(create_view_access_logging_message(request))

    form = ServerForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        instance = form.save(commit=False)

        # Custom form validations
        instance.save()
        instance.server_types.clear()
        for u in form.cleaned_data.get("server_types"):
            instance.server_types.add(u)
        messages.success(request, _("Server: {} was created successfully.").format(instance.dns))
        # return to URL
        return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})


@login_required
@staff_member_required
def server_delete(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    instance.delete()
    messages.success(request, _("Server was deleted successfully"))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@staff_member_required
def server_update(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    form = ServerForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            _server = form.save(commit=False)

            _server.save()
            _server.server_types.clear()
            for u in form.cleaned_data.get("server_types"):
                _server.server_types.add(u)
            messages.success(request, _("Server: {} was updated successfully.").format(_server.dns))
            return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})
