import csv
import itertools
import json
import logging
from types import SimpleNamespace
from typing import Any, cast

from core.models import Room, Tenant
from core.serialize import _deserialize_rooms, deserialize_data, serialize_data
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.db.transaction import atomic
from django.http import HttpResponse, StreamingHttpResponse
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def import_export(request):
    logger.info(create_view_access_logging_message(request))

    return render(request, "import_export.html")


@login_required
@staff_member_required
def import_upload_json(request):
    logger.info(create_view_access_logging_message(request))

    if request.method == "POST":
        try:
            logger.debug("request.method == 'POST'; Prepare json import")

            deserialized_data = deserialize_data(json.load(request.FILES["document"]))

            return render(request, "import_export_importsuccess.html", deserialized_data)

        except KeyError as e:
            messages.warning(request, "KeyError: make sure all necessary attributes were set: " + str(e))
            return redirect("import_export")

        except json.JSONDecodeError as e:
            messages.warning(request, "JSONDecodeError: something must be wrong with your file structure: " + str(e))
            return redirect("import_export")


@login_required
@staff_member_required
def import_upload_csv(request):
    logger.info(create_view_access_logging_message(request))

    if request.method == "POST":
        try:
            logger.debug("request.method == 'POST'; Prepare csv import")

            csv_file_content = request.FILES["document"].read().decode("utf-8")
            dialect = csv.Sniffer().sniff(csv_file_content)
            data = sorted(csv.DictReader(csv_file_content.splitlines(), dialect=dialect), key=lambda row: row["name"])

            uniqueness_violations = set(
                row["name"] for i, row in enumerate(data) if i > 0 and row["name"] == data[i - 1]["name"]
            )

            if uniqueness_violations:
                logger.debug("Render import_export.html")
                return render(request, "import_export.html", {"uniqueness_violations": uniqueness_violations})
            else:
                with atomic():
                    logger.debug("Open transaction and start csv import!")

                    created_rooms = list()
                    updated_rooms = list()
                    ignored_rooms = list()

                    _deserialize_rooms(
                        map(
                            lambda row: {
                                "name": row["name"],
                                "config_name": None,
                                "tenant": Tenant.objects.filter(name=row["tenant"]).first(),
                                "is_public": row["is_public"],
                                "comment_public": row["comment_public"],
                                "comment_private": row["comment_private"],
                                "click_counter": Room._meta.get_field("click_counter").get_default(),
                                "event_collection_strategy": Room._meta.get_field(
                                    "event_collection_strategy"
                                ).get_default(),
                                "event_collection_parameters": Room._meta.get_field(
                                    "event_collection_parameters"
                                ).get_default(),
                                "maxParticipants": Room._meta.get_field("maxParticipants").get_default(),
                            },
                            data,
                        ),
                        created_rooms,
                        updated_rooms,
                        ignored_rooms,
                    )

                    return render(
                        request,
                        "import_export_importsuccess.html",
                        {
                            "created_rooms": created_rooms,
                            "updated_rooms": updated_rooms,
                            "ignored_rooms": ignored_rooms,
                        },
                    )
        except KeyError as e:
            messages.error(request, _("KeyError: make sure all necessary attributes were set"))
            logger.warning("KeyError: make sure all necessary attributes were set " + str(e))
            return redirect("import_export")


@login_required
@staff_member_required
def export_download_json(request):
    logger.info(create_view_access_logging_message(request))
    logger.debug("Starting json export")

    # TODO inline with `StreamingHttpResponse(..., headers={...})` in Django 3.2
    response = HttpResponse(json.dumps(serialize_data(), indent=4), content_type="application/json")
    response["Content-Disposition"] = 'attachment; filename="export.json"'

    logger.debug("Json file created")
    return response


@login_required
@staff_member_required
def export_download_csv(request):
    logger.info(create_view_access_logging_message(request))
    logger.debug("Starting csv export")

    # dummy writer with a file-like object which just returns the input,
    # thus `writer.writerow` returns the formatted row.
    # See also https://docs.djangoproject.com/en/dev/howto/outputting-csv/#streaming-csv-files
    writer = csv.writer(cast(Any, SimpleNamespace(write=lambda row: row)))
    # an (unfetched) iterator with the header prepended
    rows = itertools.chain(
        [["tenant", "name", "is_public", "comment_public", "comment_private"]],
        Room.objects.values_list("tenant__name", "name", "is_public", "comment_public", "comment_private"),
    )

    # TODO inline with `StreamingHttpResponse(..., headers={...})` in Django 3.2
    response = StreamingHttpResponse((writer.writerow(row) for row in rows), content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="export.csv"'

    logger.debug("Csv file created")
    return response
