import logging

from core.filters import TenantFilter
from core.forms import TenantFilterFormHelper, TenantForm
from core.models import Tenant
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@staff_member_required
def tenant_overview(request):
    logger.info(create_view_access_logging_message(request))

    f = TenantFilter(request.GET, queryset=Tenant.objects.all())
    f.form.helper = TenantFilterFormHelper
    return render(request, "tenants_overview.html", context={"filter": f})


@login_required
@staff_member_required
def tenant_create(request):
    logger.info(create_view_access_logging_message(request))

    form = TenantForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        tenant = form.save(commit=False)

        # Custom form validations

        tenant.save()

        messages.success(request, _("Tenant was created successfully."))
        logger.debug("Tenant (%s) was created successfully.", tenant)
        # return to URL
        return redirect("tenants_overview")

    logger.debug("Tenant (%s) was not updated correctly; is_valid=%s", form.is_valid())
    return render(request, "tenants_create.html", {"form": form})


@login_required
@staff_member_required
def tenant_delete(request, tenant):
    logger.info(create_view_access_logging_message(request, tenant))

    instance = get_object_or_404(Tenant, pk=tenant)
    instance.delete()
    messages.success(request, _("Tenant was deleted successfully."))
    logger.debug("Tenant (%s) was deleted successfully", tenant)

    return redirect("tenants_overview")


@login_required
@staff_member_required
def tenant_update(request, tenant):
    logger.info(create_view_access_logging_message(request, tenant))

    instance = get_object_or_404(Tenant, pk=tenant)
    form = TenantForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _tenant = form.save(commit=False)

        _tenant.save()

        messages.success(request, _("Tenant was updated successfully."))
        logger.debug("Tenant (%s) was updated successfully", tenant)

        return redirect("tenants_overview")

    logger.debug("Tenant (%s) was NOT updated successfully", tenant)
    return render(request, "tenants_create.html", {"form": form})
