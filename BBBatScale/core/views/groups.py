from core.filters import GroupFilter
from core.forms import GroupFilterFormHelper, GroupForm
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _


@login_required
@staff_member_required
def groups_overview(request):
    f = GroupFilter(request.GET, queryset=Group.objects.all())
    f.form.helper = GroupFilterFormHelper
    return render(request, "groups_overview.html", {"filter": f})


@login_required
@staff_member_required
def group_create(request):
    form = GroupForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        group = form.save(commit=False)
        # Custom form validations

        group.save()
        messages.success(request, _("Group {} successfully created.").format(group.name))
        # return to URL
        return redirect("groups_overview")
    return render(request, "group_create.html", {"form": form})


@login_required
@staff_member_required
def group_delete(request, group):
    instance = get_object_or_404(Group, pk=group)
    instance.delete()
    messages.success(request, _("Group successfully deleted."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@staff_member_required
def group_update(request, group):
    instance = get_object_or_404(Group, pk=group)

    if request.method == "POST":
        form = GroupForm(request.POST, instance=instance)
        if form.is_valid():
            _group = form.save(commit=False)

            _group.save()

            messages.success(request, _("Group {} successfully updated").format(_group.name))
            return redirect("groups_overview")
    form = GroupForm(instance=instance, initial={"name": instance.name})
    return render(request, "group_create.html", {"form": form})
