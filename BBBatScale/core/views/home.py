import logging

from core.models import Meeting, Room, Server
from core.services import get_rooms_with_current_next_event
from core.views import create_view_access_logging_message
from django.conf import settings
from django.shortcuts import render

logger = logging.getLogger(__name__)


def home(request):
    logger.info(create_view_access_logging_message(request))

    rooms = get_rooms_with_current_next_event()
    if settings.SUPPORT_CHAT_ENABLED:
        from support_chat.models import SupportChatParameter

        disable_chat_if_offline = "true" if SupportChatParameter.load().disable_chat_if_offline else "false"
    else:
        disable_chat_if_offline = "false"

    return render(
        request,
        "home.html",
        {
            "servers": Server.objects.all(),
            "rooms": rooms,
            "participants_current": Room.get_participants_current(),
            "performed_meetings": Meeting.objects.all().count(),
            "base_url": settings.BASE_URL,
            "disable_chat_if_offline": disable_chat_if_offline,
        },
    )
