from django import template
from django.conf import settings
from django.template import RequestContext
from django.template.base import FilterExpression, Node, NodeList, Parser, Token

register = template.Library()


class IfApplicationNode(Node):
    def __init__(self, application: FilterExpression, nodelist: NodeList) -> None:
        self.application = application
        self.nodelist = nodelist

    def render(self, context: RequestContext) -> str:
        if self.application.resolve(context, ignore_failures=True) in settings.INSTALLED_APPS:
            return self.nodelist.render(context)
        return ""


@register.tag("if_app_installed")
def do_if_app_installed(parser: Parser, token: Token) -> IfApplicationNode:
    # {% if_app_installed ... %}
    bits = token.split_contents()

    if len(bits) != 2:
        raise template.TemplateSyntaxError(f"Tag '{bits[0]}' expects exactly one argument")

    application = parser.compile_filter(bits[1])

    nodelist = parser.parse(["end_if_app_installed"])
    token = parser.next_token()

    # {% end_if_app_installed %}
    if token.contents != "end_if_app_installed":
        raise template.TemplateSyntaxError(
            "Malformed template tag at line {0}: '{1}'".format(token.lineno, token.contents)
        )

    return IfApplicationNode(application, nodelist)
