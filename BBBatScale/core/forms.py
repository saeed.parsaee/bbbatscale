from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Fieldset, Layout, MultiWidgetField, Submit
from django import forms
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from .models import GeneralParameter, PersonalRoom, Room, RoomConfiguration, Server, ServerType, Tenant, User


class GeneralParametersForm(forms.ModelForm):
    class Meta:
        model = GeneralParameter
        fields = [
            "latest_news",
            "jitsi_url",
            "faq_url",
            "feedback_email",
            "footer",
            "logo_link",
            "favicon_link",
            "page_title",
            "home_room_enabled",
            "playback_url",
            "home_room_teachers_only",
            "home_room_tenant",
            "enable_occupancy",
            "download_url",
            "home_room_room_configuration",
            "jitsi_enable",
            "default_theme",
            "personal_rooms_enabled",
            "personal_rooms_tenant",
            "personal_rooms_teacher_max_number",
            "personal_rooms_non_teacher_max_number",
        ]

    def __init__(self, *args, **kwargs):
        super(GeneralParametersForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-3"
        self.helper.field_class = "col-lg-9"
        self.helper.layout = Layout(
            Field("latest_news"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("General site settings"))
            ),
            Field("page_title"),
            Field("logo_link"),
            Field("favicon_link"),
            Field("default_theme"),
            Field("jitsi_enable"),
            Field("jitsi_url"),
            Field("faq_url"),
            Field("playback_url"),
            Field("download_url"),
            Field("footer"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Home room"))
            ),
            Field("home_room_enabled"),
            Field("home_room_teachers_only"),
            Field("home_room_tenant"),
            Field("home_room_room_configuration"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Personal room"))
            ),
            Field("personal_rooms_enabled"),
            Field("personal_rooms_tenant"),
            Field("personal_rooms_teacher_max_number"),
            Field("personal_rooms_non_teacher_max_number"),
            Field("enable_occupancy"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = [
            "dns",
            "datacenter",
            "shared_secret",
            "tenant",
            "participant_count_max",
            "videostream_count_max",
            "server_types",
        ]

    def __init__(self, *args, **kwargs):
        super(ServerForm, self).__init__(*args, **kwargs)

        self.fields["dns"].label = _("DNS")
        self.fields["tenant"].label = _("Tenant")
        self.fields["datacenter"].label = _("Datacenter")
        self.fields["shared_secret"].label = _("Shared Secret")
        self.fields["shared_secret"].required = True
        self.fields["participant_count_max"].label = _("Max participants")
        self.fields["videostream_count_max"].label = _("Max videostreams")
        self.fields["server_types"].label = _("Server Types")
        self.fields["server_types"].queryset = ServerType.objects.all()
        self.fields["server_types"].help_text = _(
            "Server types are determining which work is scheduled on that server."
        )

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("dns"),
            Field("tenant"),
            Field("datacenter"),
            Field("shared_secret"),
            MultiWidgetField("server_types"),
            Field("participant_count_max"),
            Field("videostream_count_max"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "tenant",
            "name",
            "config",
            "is_public",
            "comment_private",
            "comment_public",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)

        self.fields["tenant"].label = _("Tenant")
        self.fields["name"].label = _("Name")
        self.fields["config"].label = _("Configuration")
        self.fields["is_public"].label = _("Public")
        self.fields["comment_private"].label = _("Comment private")
        self.fields["comment_public"].label = _("Comment public")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("tenant"),
            Field("name"),
            Field("config"),
            Field("is_public"),
            Field("comment_private"),
            Field("comment_public"),
            Field("maxParticipants"),
            Field("streamingUrl"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class HomeRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ["name", "config", "is_public", "comment_public"]

    def __init__(self, *args, **kwargs):
        super(HomeRoomForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["config"].label = _("Configuration")
        self.fields["is_public"].label = _("Visible on front page")
        self.fields["comment_public"].label = _("Comment")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name", readonly=True, diabled=True),
            Field("config"),
            Field("is_public"),
            Field("comment_public"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PersonalRoomForm(forms.ModelForm):
    class Meta:
        model = PersonalRoom
        fields = ["name", "config", "is_public", "comment_public", "co_owners"]

    def __init__(self, *args, **kwargs):
        if "instance" in kwargs:
            self.instance = kwargs.get("instance")
        if "co_qs" in kwargs:
            self.co_qs = kwargs.pop("co_qs")
        super(PersonalRoomForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["config"].label = _("Room configuration")
        self.fields["is_public"].label = _("Visible on front page")
        self.fields["comment_public"].label = _("Comment")
        self.fields["co_owners"].label = _("Co-Owners")
        if self.co_qs:
            self.fields["co_owners"].queryset = User.objects.all()
        else:
            self.fields["co_owners"].queryset = (
                User.objects.none() if not self.instance.pk else self.instance.co_owners.all()
            )

        self.helper: FormHelper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("config"),
            Field("is_public"),
            Field("comment_public"),
            HTML(
                '<hr><div class="form-group row "><label for="id_filter_owner" '
                'class="col-form-label col-lg-4">Filter Users</label>'
                '<div class="col-lg-8"><input type="text" name="filter_owner" '
                'class="textinput textInput form-control" id="id_filter_owner"></div></div>'
            ),
            MultiWidgetField("co_owners"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PersonalRoomCoOwnerForm(forms.ModelForm):
    class Meta:
        model = PersonalRoom
        fields = ["owner", "name", "config", "is_public", "comment_public", "co_owners"]

    def __init__(self, *args, **kwargs):
        if "instance" in kwargs:
            self.instance = kwargs.get("instance")
        super(PersonalRoomCoOwnerForm, self).__init__(*args, **kwargs)

        self.fields["owner"].label = _("Owner")
        self.fields["owner"].required = False
        self.fields["name"].label = _("Name")
        self.fields["name"].required = False
        self.fields["config"].label = _("Room configuration")
        self.fields["is_public"].label = _("Visible on front page")
        self.fields["comment_public"].label = _("Comment")
        self.fields["co_owners"].id = "id_co-owners_read_only"
        self.fields["co_owners"].queryset = (
            User.objects.none() if not self.instance.pk else self.instance.co_owners.all()
        )

        self.helper: FormHelper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("owner", readonly=True, disabled=True),
            Field("name", readonly=True, disabled=True),
            Field("config"),
            Field("is_public", readonly=True, disabled=True),
            Field("comment_public", readonly=True, disabled=True),
            Field("co_owners", readonly=True, disabled=True, id="id_co-owners_read_only"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomConfigurationForm(forms.ModelForm):
    class Meta:
        model = RoomConfiguration
        fields = [
            "name",
            "mute_on_start",
            "all_moderator",
            "everyone_can_start",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "authenticated_user_can_start",
            "guest_policy",
            "dialNumber",
            "welcome_message",
            "logoutUrl",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(self, *args, **kwargs):
        super(RoomConfigurationForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["all_moderator"].label = _("Everyone is moderator")
        self.fields["everyone_can_start"].label = _("Everyone can start meeting")
        self.fields["authenticated_user_can_start"].label = _("Registered user can start meeting")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non moderators")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["guest_policy"].label = _("Enable Guestlobby")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("name"),
            Field("mute_on_start"),
            Field("all_moderator"),
            Field("everyone_can_start"),
            Field("authenticated_user_can_start"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_recording"),
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            Field("dialNumber"),
            Field("maxParticipants"),
            Field("streamingUrl", placeholder="rtmp://streaming.server.tld/url/key"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class TenantForm(forms.ModelForm):
    class Meta:
        model = Tenant
        fields = ["name", "scheduling_strategy", "description", "notifications_emails"]

    def __init__(self, *args, **kwargs):
        super(TenantForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        self.fields["description"].label = _("Description")
        self.fields["notifications_emails"].label = _("Email Notifications")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("scheduling_strategy"),
            Field("description"),
            Field("notifications_emails"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ConfigureRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "mute_on_start",
            "all_moderator",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "guest_policy",
            "welcome_message",
            "logoutUrl",
            "dialNumber",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(self, *args, **kwargs):
        super(ConfigureRoomForm, self).__init__(*args, **kwargs)

        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["all_moderator"].label = _("Everyone is moderator")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non-moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non-moderators")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["guest_policy"].label = _("Enable Guestlobby")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("mute_on_start"),
            Field("all_moderator"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_recording"),
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            Field("dialNumber"),
            Field("maxParticipants"),
            Field("streamingUrl", placeholder="rtmp://streaming.server.tld/url/key"),
            Submit("save", _("Start"), css_class="btn-primary"),
        )


class RoomFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                Div("tenant", css_class="col"),
                Div("server", css_class="col"),
                Div("config", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class ServerFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("tenant", css_class="col"),
                Div("dns", css_class="col"),
                Div("state", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class TenantFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class GroupFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email", "groups", "is_staff", "password"]

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        self.fields["first_name"].label = _("First name")
        self.fields["last_name"].label = _("Last name")
        self.fields["groups"].label = _("Groups")
        self.fields["is_staff"].label = _("Staff status")
        self.fields["password"].label = _("Password")
        self.fields["email"].label = _("Email")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username"),
            Field("password"),
            Field("first_name"),
            Field("last_name"),
            Field("email"),
            Field("groups"),
            Field("is_staff"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "groups", "email", "is_staff", "personal_rooms_max_number"]

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        self.fields["first_name"].label = _("First name")
        self.fields["last_name"].label = _("Last name")
        self.fields["groups"].label = _("Groups")
        self.fields["is_staff"].label = _("Staff status")
        self.fields["email"].label = _("Email")
        self.fields["personal_rooms_max_number"].label = _("Maximum number of personal rooms")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username"),
            Field("first_name"),
            Field("last_name"),
            Field("email"),
            Field("groups"),
            Field("is_staff"),
            Field("personal_rooms_max_number"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserResetPasswordAdminForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username"]

    password = forms.CharField(min_length=8, help_text=_("Minimum password length 8 character"))
    password_confirm = forms.CharField(min_length=8, help_text=_("Confirm password so they match"))

    def __init__(self, *args, **kwargs):
        super(UserResetPasswordAdminForm, self).__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        self.fields["password"].label = _("New password")
        self.fields["password_confirm"].label = _("New password confirm")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username", readonly=True, diabled=True),
            Field("password"),
            Field("password_confirm"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PasswordChangeCrispyForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChangeCrispyForm, self).__init__(*args, **kwargs)
        self.fields["old_password"].label = _("Old password")
        self.fields["new_password1"].label = _("New password")
        self.fields["new_password2"].label = _("Repeat new password")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("old_password"),
            Field("new_password1"),
            Field("new_password2"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )
