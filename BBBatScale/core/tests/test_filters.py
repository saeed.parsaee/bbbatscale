from core.constants import SERVER_STATE_DISABLED, SERVER_STATE_UP
from core.filters import RoomFilter, ServerFilter, TenantFilter
from core.models import Room, RoomConfiguration, Server, Tenant
from django.http import QueryDict
from django.test import TestCase


class CoreFilterTestCase(TestCase):
    def setUp(self) -> None:
        self.fbi = Tenant.objects.create(name="FBI")
        self.hda = Tenant.objects.create(name="HDA")
        self.room_config1 = RoomConfiguration.objects.create(name="Config1")
        self.room_config2 = RoomConfiguration.objects.create(name="Config2")
        self.bbb1_server = Server.objects.create(
            dns="bbb1.example.org", datacenter="fbi", tenant=self.fbi, state=SERVER_STATE_UP
        )
        self.bbb2_server = Server.objects.create(
            dns="bbb2.other.example.org", datacenter="fbi1337", tenant=self.fbi, state=SERVER_STATE_DISABLED
        )
        self.bbb3_server = Server.objects.create(
            dns="bbb3.other.example.org", datacenter="hda", tenant=self.hda, state=SERVER_STATE_UP
        )
        self.room1 = Room.objects.create(name="D14/00.01", tenant=self.fbi, config=self.room_config1)
        self.room2 = Room.objects.create(name="D15/00.01", tenant=self.fbi, config=self.room_config2)
        self.room3 = Room.objects.create(
            name="Mathlab", tenant=self.hda, config=self.room_config2, server=self.bbb2_server
        )

    def test_room_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"name": "/"})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(name__icontains="/"), transform=lambda x: x)
        get_params.pop("name")
        get_params.update({"tenant": self.fbi.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(tenant=self.fbi.pk), transform=lambda x: x)
        get_params.pop("tenant")
        get_params.update({"server": self.bbb2_server.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(server=self.bbb2_server.pk), transform=lambda x: x)
        get_params.pop("server")
        get_params.update({"config": self.room_config2.pk})
        f = RoomFilter(get_params, Room.objects.all())
        self.assertQuerysetEqual(f.qs, Room.objects.filter(config=self.room_config2.pk), transform=lambda x: x)

    def test_server_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"dns": "other.example.org"})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(dns__icontains="other.example.org"), transform=lambda x: x)
        get_params.pop("dns")
        get_params.update({"tenant": str(self.hda.pk)})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(tenant=self.hda.pk), transform=lambda x: x)
        get_params.pop("tenant")
        get_params.update({"dns": "fbi1337"})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(datacenter__icontains="fbi1337"), transform=lambda x: x)
        get_params.pop("dns")
        get_params.update({"state": SERVER_STATE_UP})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(state=SERVER_STATE_UP), transform=lambda x: x)

    def test_tenant_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"name": "HDA"})
        f = TenantFilter(get_params, Tenant.objects.all())
        self.assertQuerysetEqual(f.qs, Tenant.objects.filter(name__icontains="HDA"), transform=lambda x: x)
