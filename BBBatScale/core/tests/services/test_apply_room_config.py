import pytest
from core.models import Room, Tenant
from core.services import apply_room_config


@pytest.fixture(scope="function")
def example(db) -> Tenant:
    return Tenant.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def room_d14_0303(db, example) -> Room:
    return Room.objects.create(
        tenant=example,
        meeting_id="12345678",
        name="D14/03.03",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        all_moderator=False,
    )


def test_apply_room_config(room_d14_0303):
    apply_room_config(room_d14_0303.meeting_id, room_d14_0303.config)
    guest_policy = Room.objects.get(meeting_id=room_d14_0303.meeting_id).guest_policy
    assert guest_policy == "ALWAYS_ACCEPT"
