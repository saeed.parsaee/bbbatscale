from core.services import moderator_message
from django.conf import settings


def test_moderator_message():
    message = moderator_message("test_user", "123", True, "ASK_MODERATOR", 10)
    expected_message = (
        "Meeting link: " + settings.BASE_URL + "/r/test_user<br/>"
        "This room is protected by an access code only for guests: 123<br/>"
        "The room has a guest lobby enabled<br/>"
        "The room is limited to 10 participants"
    )
    message_without_guest_lobby_and_access_codes = moderator_message("test_user", "", False, "ALWAYS_DENY", 10)
    expected_message_without_guest_lobby_and_access_codes = (
        "Meeting link: " + settings.BASE_URL + "/r/test_user<br/>The room is limited to 10 participants"
    )
    message_max_partitipants_none = moderator_message("test_user", "123", False, "ASK_MODERATOR", None)
    expected_message_participants_none = (
        "Meeting link: " + settings.BASE_URL + "/r/test_user<br/>"
        "This room is protected by an access code: 123<br/>"
        "The room has a guest lobby enabled"
    )
    assert message_max_partitipants_none == expected_message_participants_none
    assert message == expected_message
    assert message_without_guest_lobby_and_access_codes == expected_message_without_guest_lobby_and_access_codes
