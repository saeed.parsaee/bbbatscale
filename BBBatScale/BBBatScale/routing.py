from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf import settings
from django.urls import path

application_mapping = {}

websocket_urlpatterns = []
websocket_urlrouter = None

if settings.SUPPORT_CHAT_ENABLED:
    import support_chat.routing

    websocket_urlpatterns.append(path("supportchat/", URLRouter(support_chat.routing.websocket_urlpatterns)))

if websocket_urlpatterns:
    websocket_urlrouter = URLRouter([path("ws/", URLRouter(websocket_urlpatterns))])
    application_mapping["websocket"] = AuthMiddlewareStack(websocket_urlrouter)

application = ProtocolTypeRouter(application_mapping)
