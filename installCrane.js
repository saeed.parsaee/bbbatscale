const fetch = require("node-fetch");
const tar = require("tar");

async function installCrane() {
    const response = await fetch("https://api.github.com/repos/google/go-containerregistry/releases/latest");
    const asset = (await response.json())["assets"].find(
        (asset) => asset.name === "go-containerregistry_Linux_x86_64.tar.gz"
    );
    if (!asset) {
        throw new Error("Unable to find linux-x86-64 release asset");
    }

    const downloadResponse = await fetch(asset["browser_download_url"]);
    downloadResponse.body.pipe(tar.extract({ cwd: "/usr/local/bin" }, ["crane"]));
}

installCrane().catch((error) => {
    console.error(error);
    process.exit(1);
});
