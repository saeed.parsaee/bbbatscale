# Getting Started

The following steps describe how to get BBB@Scale up and running.
The sections are split into: prerequisites, installation, configuration and administration.

If you just want to play around and get first impressions of the tool check out our [Demo](http://demo.bbbatscale.de)
(Username=admin | Password=bbbATscale). The Demo resets its configuration at 00:00 CEST.

If you have any questions which are not answered in the [ReadMe](README.md) or this [Getting Started Guide](GettingStarted.md), feel free to send us a mail at mail@bbbatscale.de.
If you want send us an encrypted email, use the pgp-key under
([PGP-Key](mailAtBBBatScale.de_public.pgp)).

## Architecture & Overview

The following architectural overview describes the basic components and procedures involved in a BBB@Scale setup.
![Architecture](/Documentation/architecture.png?raw=true 'Architectural overview')

1. A users visits the BBB@Scale web page and selects a room.
2. BBB@Scale schedules a room on a appropriate BBB instance whether it is on premise or in the cloud.
3. BBB@Scale redirects the user to the desired room, after its creation is confirmed.

## Prerequisites

For a minimal working installation of BBB@Scale you need

- 1x Server (Physical, Virtual) with Linux (we use Ubuntu 20.04) and Docker enabled
- 1x BBB Server with [bbb-webhooks](https://docs.bigbluebutton.org/dev/webhooks.html) enabled

### Example setup

Check out our [example setup](/Documentation/setup_example.md) to get an idea how we operate our productive
BBB@Scale setup

## Installation (Standalone, Minimal)

We are using a VM with Ubuntu 20.04. Please make sure that it is reachable via TCP 80 and you have a working docker setup.
Digital Ocean has a great [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-de) on how to setup docker.

**Step 1** \
Navigate to the path where you want the project to be located. In our case, we chose `/`.
If you choose to use another path, remember to adjust the paths in the following commands.
```shell
cd /
```

**Step 2** \
Clone the repository.
```shell
git clone git@gitlab.com:bbbatscale/bbbatscale.git
```

**Step 3** \
Copy the dummy env files for the docker containers inside `/bbbatscale/configurations/env/`.
```shell
cp /bbbatscale/configurations/env/bbbatscale.env.dummy /bbbatscale/configurations/env/bbbatscale.env
cp /bbbatscale/configurations/env/nginx.env.dummy /bbbatscale/configurations/env/nginx.env
cp /bbbatscale/configurations/env/postgres.env.dummy /bbbatscale/configurations/env/postgres.env
```

**Step 4** \
Copy or link the server TLS certificates to `/bbbatscale/configurations/nginx/fullchain.pem` and `/bbbatscale/configurations/nginx/privatekey.pem`.

**Step 5** \
Adjust the `/bbbatscale/configurations/env/bbbatscale.env` environment variables to your needs.
Be sure that you created random secrets for `DJANGO_SECRET_KEY`, `RECORDINGS_SECRET` and set the correct `BASE_URL`.

**Step 6** \
Adjust the `/bbbatscale/configurations/env/nginx.env` environment variables to your needs.

**Step 7** \
Adjust the `/bbbatscale/configurations/env/postgres.env` environment variables to your needs.
Be sure that you created a random secret for `POSTGRES_PASSWORD`.

**Step 8** \
Start the containers.
```shell
docker-compose -f /bbbatscale/docker-compose.yml up -d
```

**Step 9** \
Create an admin user.
```shell
docker-compose -f /bbbatscale/docker-compose.yml run --rm bbbatscale ./manage.py createsuperuser
```

**Step 10** \
Add the following lines to your crontab.
```
*/1 * * * * docker-compose -f /bbbatscale/docker-compose.yml run --rm -T bbbatscale bbbatscale collect-server-stats
*/3 * * * * docker-compose -f /bbbatscale/docker-compose.yml run --rm -T bbbatscale bbbatscale house-keeping
```

**Step 11** \
Open your browser, navigate to http://*YourIP* and enjoy,

**Step 12** - Optional LDAP \
LDAP authentication requires a
[django-auth-ldap](https://django-auth-ldap.readthedocs.io/en/latest/index.html)
configuration file describing your local LDAP setup.  To enable LDAP
authentication create a file named `ldap_config.py` in the Django settings
directory (see
[here](https://django-auth-ldap.readthedocs.io/en/latest/example.html) for an
example).

When using docker-compose, make the locally stored file available to the
`django` container at `/django-project/BBBatScale/settings/ldap_config.py`
using an override file or by editing `docker-compose.yml`.

See the [django-auth-ldap docs](https://django-auth-ldap.readthedocs.io/en/latest/example.html) how
to grant admin and staff privileges to existing LDAP groups.
Use the `BBBATSCALE_MODERATORS_GROUP` environment variable to specify the LDAP
group which grants members moderator privileges within BBB@Scale.

**Step 13** - some notes on Logging \
By default BBB@Scale provides two formatters for logging in one-line messages on Stdout out of the box. 
Further, whenever possible BBB@Scale logs contain a *correlation_id* to make debugging easier (it is generated using [Django GUID](ttps://django-guid.readthedocs.io/en/latest/)).
It is a unique ID to all log outputs for every requests that is handled. The ID is also exchanged between the server 
and the users browser as part of the http header. 

You can customize logging similar to step 10, by behavior by creating a file named `logging_config.py` in the Django settings
directory.
Within this file, you need to define a python dict called `LOGGING` according to the [Logging Configuration Dictonary Schema](https://docs.python.org/3/library/logging.config.html).
BBB@Scales default logging configuration will completely be overwritten if a custom configuration is provided. 

There are two preconfigured capabilities within BBB@Scale to extend the default behavior:
1. BBB@Scale brings along the ability to forward logs to [GrayLog](https://www.graylog.org/) using [graypy](https://github.com/severb/graypy).
1. BBB@Scale brings along the ability to format log outputs in JSON format. This is done using [python-json-logger](https://github.com/madzak/python-json-logger).
However, if you want to use it, you will have to provide a configuration in the previously mentioned `logging_config.py`.

The following is an example of `logging_config.py` file for doing one-line logging to stdout and json based logging to a GrayLog server.
Besides that, it is based on the standard logging configuration of BBB@Scale.
```python
log_format = '%(levelno)s, ' \
                 '%(levelname)s, ' \
                 '%(asctime)s, ' \
                 '%(correlation_id)s, ' \
                 '%(module)s, ' \
                 '%(filename)s, ' \
                 '%(funcName)s, ' \
                 '%(lineno)s, ' \
                 '%(process)s, ' \
                 '%(processName)s, ' \
                 '%(thread)s, ' \
                 '%(threadName)s, ' \
                 '%(message)s'

LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'correlation_id': {
                '()': 'django_guid.log_filters.CorrelationId'
            }
        },
        'formatters': {
            'verbose': {
                'format': log_format,
            },
            'json_verbose': {
                '()': 'pythonjsonlogger.jsonlogger.JsonFormatter',
                'format': log_format,
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
                'filters': ['correlation_id'],
            },
            'console_json': {
                'class': 'logging.StreamHandler',
                'formatter': 'json_verbose',
                'filters': ['correlation_id'],
            },
            'graypy': {
                'class': 'graypy.GELFUDPHandler',
                'host': 'your host adress',
                'port': 12202,
                'formatter': 'json_verbose',
            },
        },
        'root': {
            'handlers': ['console', 'graypy'],
            'level': 'INFO',
        }
    }
```
For a more detailed documentation on how to configure [python-json-logger](https://github.com/madzak/python-json-logger) 
and [graypy](https://github.com/severb/graypy), please refer to the official documentation.


## Installation (OpenShift, Kubernetes)

This part will be ready soon.

## Update
Just update the project files via git pull, update/rebuild images and restart changed containers.
```shell
git -C /bbbatscale pull
docker-compose -f /bbbatscale/docker-compose.yml pull
docker-compose -f /bbbatscale/docker-compose.yml build
docker-compose -f /bbbatscale/docker-compose.yml up -d --remove-orphans
```

## Administration

After getting the installation up and running, you are ready to fill up BBB@Scale with your specific settings.

Follow the steps below:

- Create a Tenant. We assign and schedule BBB-Servers and rooms specific to one tenant. For this, you need to have at least one tenant in your setup.
  - Name: You need to provide a unique name, which will be shown in the rooms portal later on.
  - Scheduling Strategy: You have a choice between "Least participants" and "Least utilization". Default is set to "Least utilization". Least participants schedules based on the number of users in a meeting. Least utilization schedules based on a calculation of system usage (Audio and video users).

### Tenants screen. Listing all available tenants and create new ones

![Tenants](/Documentation/Screenshots/7.create_tenant.png?raw=true 'Tenants')

- Create Room Types
  You need to predefine at least one Room Type.
  - Name: You need to provide a unique name, which will be shown in the rooms portal later on.

### Room Types screen. Listing all available Room Types and create new ones

![RoomTypes](/Documentation/Screenshots/8.create_roomstypes.png?raw=true 'RoomTypes')

- Register server
  You need to have BigBlueButton servers installed in your deployment. Take a look at [bbb-install]https://github.com/bigbluebutton/bbb-install. We recommend a small BBB Setup without the need to install Greenlight.
  - DNS: Insert your BBB Server DNS here.
  - Tenant: Select the apropriate tenant previously created.
  - Datacenter: This is optional.
  - Shared Secret: Insert the output of the section named Secret `bbb-conf --secret` here.
  - Max participants: Is used for calculation of utilization, should represent the capability of our BBB server. We will not set a hard limit in BBB.
  - Max videostreams: Is used for calculation of utilization, should represent the capability of our BBB server. We will not set a hard limit in BBB.

### Server screen. Listing all available BBB-Server and create new ones

![ServerScreen](/Documentation/Screenshots/9.register_bbbserver.png?raw=true 'ServerScreen')

- Create the needed Rooms
  Feel free to name them as you wish. For our needs, we named a lot of rooms based on existing physical buildings to facilitate virtual and real life scenarios.
  - Tenant and Name: Defines the tenant and name.
  - Configuration: Defines the preset for the specific room.
  - Public: Mark this if the room should be visible on the startpage.
  - Comment private: For an internal view of comments in Rooms.
  - Comment public: For an external view of comments in Home.

### Rooms screen. Listing all available Rooms and create new ones

![Rooms](/Documentation/Screenshots/10.create_rooms.png?raw=true 'Rooms')
