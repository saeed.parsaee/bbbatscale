# Nginx

[Nginx](https://nginx.org/en/) is a web server, reverse proxy, and e-mail proxy.

## Prerequisites

This documentation relates to our project build [Nginx Docker Image](https://hub.docker.com/r/bbbatscale/nginx).

## Configuration

The configuration is kept fairly simple, and the complex routing is build into the docker image.

### `NGINX_SERVER_NAME` - **Required**

Specifies the host name nginx should listen on.
If nginx should listen on any host name, `_` can be supplied.
Multiple host names are supported and must be split via a whitespace.
See the [nginx documentation](https://nginx.org/en/docs/http/ngx_http_core_module.html#server_name) for more information.

### `NGINX_INTERNAL_BBBATSCALE_URL` - **Required**

Specifies the address nginx should proxy non-static requests to.
Within docker, it might be something like `http://bbbatscale:8000`.

### `NGINX_CONFIG`

Specifies which config version to use.
If this is not supplied, it defaults to `http`.
Available versions are:
- `http`
- `https`
- `shibboleth-http`
- `shibboleth-https`

If you intend to use an `https` version, you have to supply the certificate at `/etc/nginx/cert/fullchain.pem` and the private key at `/etc/nginx/cert/privatekey.pem` inside the docker container.
For mor information on how to use shibboleth, see [here](./shibboleth.md).

### `NGINX_X_FORWARDED_PROTO`

Specifies the content of the X-Forwarded-Proto header send to BBB@Scale.
This might be useful if Nginx is behind a reverse proxy/load balancer.
The value of this might be a Nginx variable like `$http_x_forwarded_proto`.
If this is not supplied, it defaults to `$scheme` (the scheme with which Nginx has been requested).

### Additional Server Configurations

If you'd like to extend the existing configurations inside the server block, you could pass them as file to `/etc/nginx/include/additional_server_config`,
For example you could use this to resolve the real ip of a client if nginx is behind a known proxy.
This could look like the following example:
```nginx configuration
set_real_ip_from  192.168.1.0/24;
set_real_ip_from  192.168.2.1;
set_real_ip_from  2001:0db8::/32;
real_ip_header    X-Forwarded-For;
real_ip_recursive on;
```
For more information on how to set up the `ngx_http_realip_module`, see [their documentation](https://nginx.org/en/docs/http/ngx_http_realip_module.html).
