"use strict";

const { releaseRules, sectionMap } = require("./commit-release-rules");

const chartPath = "charts/bbbatscale/Chart.yaml";

module.exports = {
    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
                releaseRules: releaseRules,
                parserOpts: {
                    noteKeywords: ["BREAKING CHANGE", "BREAKING-CHANGE"]
                }
            }
        ],
        [
            "@semantic-release/release-notes-generator",
            {
                preset: "conventionalcommits",
                parserOpts: {
                    noteKeywords: ["BREAKING CHANGE", "BREAKING-CHANGE"]
                },
                writerOpts: {
                    commitsSort(lhs, rhs) {
                        let result = (lhs.scope || "").localeCompare(rhs.scope || "");
                        if (!lhs.scope || !rhs.scope) {
                            result = -result;
                        }

                        if (result === 0) {
                            result = (lhs.subject || "").localeCompare(rhs.subject || "");
                            if (!lhs.subject || !rhs.subject) {
                                result = -result;
                            }
                        }
                        return result;
                    }
                },
                presetConfig: {
                    types: sectionMap
                }
            }
        ],
        [
            "@semantic-release/gitlab",
            {
                gitlabUrl: "https://gitlab.com/"
            }
        ],
        {
            prepare(_, { nextRelease }) {
                const yaml = require("js-yaml");
                const fs = require("fs");

                const chart = yaml.load(fs.readFileSync(chartPath).toString(), { schema: yaml.FAILSAFE_SCHEMA });
                chart.appVersion = nextRelease.version;
                fs.writeFileSync(chartPath, yaml.dump(chart));
            },

            publish(_, { nextRelease }) {
                craneLogin(process.env.CI_REGISTRY_USER, process.env.CI_REGISTRY_PASSWORD, process.env.CI_REGISTRY);
                copyImages(
                    nextRelease.version,
                    process.env.BBBATSCALE_IMAGE,
                    process.env.NGINX_IMAGE,
                    process.env.SHIBBOLETH_SP_IMAGE
                );

                const remoteRegistry = process.env.REMOTE_REGISTRY;
                const remoteRegistryUsername = process.env.REMOTE_REGISTRY_USERNAME;
                const remoteRegistryPassword = process.env.REMOTE_REGISTRY_PASSWORD;
                if (remoteRegistry && remoteRegistryUsername && remoteRegistryPassword) {
                    craneLogin(remoteRegistryUsername, remoteRegistryPassword, remoteRegistry);
                    copyImages(
                        nextRelease.version,
                        process.env.REMOTE_BBBATSCALE_IMAGE,
                        process.env.REMOTE_NGINX_IMAGE,
                        process.env.REMOTE_SHIBBOLETH_SP_IMAGE
                    );
                }
            }
        },
        [
            "@semantic-release/git",
            {
                assets: [chartPath],
                message:
                    "release(<%= nextRelease.version %>): auto generated release version <%= nextRelease.version %> at <%= new Date().toISOString() %>\n\n<%= nextRelease.notes %>"
            }
        ]
    ],
    tagFormat: "${version}"
};

function crane(...args) {
    console.log(require("child_process").execFileSync("/usr/local/bin/crane", args, { encoding: "utf8" }));
}

function craneLogin(username, password, registry) {
    crane("auth", "login", "--username", username, "--password", password, registry);
}

function craneCopy(from, to) {
    crane("copy", from, to);
}

function copyImages(version, bbbAtScaleImage, nginxImage, shibbolethImage) {
    if (bbbAtScaleImage) {
        craneCopy(`${process.env.BBBATSCALE_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${bbbAtScaleImage}:latest`);
        craneCopy(`${process.env.BBBATSCALE_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${bbbAtScaleImage}:${version}`);
    }

    if (nginxImage) {
        craneCopy(`${process.env.NGINX_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${nginxImage}:latest`);
        craneCopy(`${process.env.NGINX_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${nginxImage}:${version}`);
    }

    if (shibbolethImage) {
        craneCopy(`${process.env.SHIBBOLETH_SP_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${shibbolethImage}:latest`);
        craneCopy(`${process.env.SHIBBOLETH_SP_IMAGE}:${process.env.CI_COMMIT_SHA}`, `${shibbolethImage}:${version}`);
    }
}
